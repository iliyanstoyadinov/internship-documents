# Explanation how to assemble Nokia 7750 SR-a4 Router:

![Router](Nokia_7750_SR-a4.jpg)

* Chassis - is a box, where we build our service provider router. Its size defines the number of modules, which we can insert in it.
* 1 - These are empty slots and we need to put fillers. The air passes through them.
* 2 - Module that is installable into certain type of line card (IOM (3 on the picture)). Nokia called this module Media Dependent Adapter (MDA). 
There are different types of such MDA: 1/10/40/100G, STM and so on. 
Certain services (i.E NAT) demands additional modules, because they aren’t being processed by line card or control board directly.
Nokia (Alcatel-Lucent) calls such module Multiservice Integrated Service Module 2 (MS-ISA2), which can be inserted into IOM. 
There is also dedicated line card, which is called Multiservice Integrated Service Module (MS-ISM) and have two integrated MS-ISA2 on board.
* 3 - Line card is the board, which is used for connecting interfaces and performing traffic forwarding.
Line card is a Cisco’s term, which is being actively used by other vendors as well.  
Nokia (Alcatel-Lucent) has two different types of line cards: Integrated Media Module (IMM) is a line card with fixed number/type of
ports and Input/Output Module (IOM) is a line card with possibility to install modules, which actually have interface.
At one IOM it’s possible to install such modules. In this case we have IOM inserted.
* 4 - Control board is a “brain” of the router, which is responsible for all control plane (OSPF/IS-IS/BGP/PIM/LDP/RSVP-TE/etc) 
and management plane (SSH/Telnet/SNMP/etc) operation. Nokia (Alcatel-Lucent) calls this module Control Processing Module (CPM). 
In critical environment two CPM/RSP installed into the service provider router. Dual CPM/RSP also doubles performance of the router backplane (see the next point).
In our case we have only 1 CPM and filler on the other possible slot for CPM.
* 5 - Power supply - We have 2 power supplies
* 6 - Fans or Fan Tray. There are 4 Fans that cools all other components.