# Candidate Configuration:

* `A:vRR# candidate edit`

* After configuring we can see what we did in the router: `A:vRR>edit-cfg# candidate view`

* And we can commit the candidate configuration: 
```
A:vRR>edit-cfg# candidate commit
Processing current config… 0.000 s
Saving checkpoint file… OK
INFO: CLI Successfully executed 17 lines in 0.000 s.
*A:vRR#
```

* You can issue “candidate commit timeout X”, where X is the amount of minutes, after which the configuration will be automatically reverted back.

* Rollback to the previous config: `A:SR1# admin rollback revert 1`

* Don’t forget to save your configuration! After the commit only the active (running) configuration is changed.