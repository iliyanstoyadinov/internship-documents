# Subinterfaces and VLANs:

```
A:SR3# admin display-config
 #——————————–
echo “Port Configuration”
#——————————–
port 1/1/1
 ethernet
encap-type dot1q
 exit
 no shutdown
 exit
 #——————————–
echo “IP Configuration”
#——————————–
interface “toSR_p1:v11”
address 172.16.11.1/24
port 1/1/1:11
 ipv6
 address fc00::172:16:11:1/120
 exit
 no shutdown
 exit
 interface “toSR_p1:v12”
address 172.16.12.1/24
port 1/1/1:12
 ipv6
 address fc00::172:16:12:1/120
 exit
 no shutdown
 exit
#——————————–
```