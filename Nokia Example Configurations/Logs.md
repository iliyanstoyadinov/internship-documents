# Logs Configuration:

## Four main destinations, where we can send logs locally:

* console - Logs are sent to CLI (command line interface), where you can observe them in real-time. In this case console line itself (serial or USB console).
* session - Logs are sent to CLI, which now relates to ssh/telnet connections.
* memory - Logs are stored in router’s RAM. 
* file-id - Logs are stored in file on flash/HDD. They are resistant to reboot of the device.

## Four type of log sources:

* “Main” - relates to all usual events that happens in Nokia (Alcatel-Lucent) SR operation (i.e. establishing IGP adjacency, booting of card, flapping of ports, etc).
* “Change” - means the changes done in the configuration of router.
* “Security” - relates to security breaching events.
* “Debug-trace” - covers all the low level information (BGP update packets, ARP request/reply, etc).

```
*A:SR3>config>log# info
log-id 1
 description “DEBUG_TO_CONSOLE”
time-format local
 from debug-trace
 to console
 no shutdown
 exit
```

* This configuration of log stream will send all low level information (and only it) to the console.
* The provided configuration sends only debug information to console.
If you want to send also information about other events (“main”, “change” or “security”), 
you have to configure separate log streams.

### Send to the console also information about ports flapping (going up or down):

```
*A:SR3>config>log# info
filter 1
 default-action drop
 entry 1
 action forward
 match
 message eq pattern “Interface”
exit
 exit
 exit
 log-id 2
 filter 1
 from main
 to session
 no shutdown
 exit
```

* !!! Overall logic for Nokia (Alcatel-Lucent) SR OS log stream is: one source, one destination and one filter per stream. !!!

### Debug OSPF to ssh/telnet remtoe connection:

```
*A:SR3>config>log# info
filter 11
 default-action drop
 description “OSPF”
entry 1
 action forward
 match
 application eq “ospf”
exit
 exit
 exit
 log-id 3
 description “OSPF_TO_TERMINAL”
filter 11
 from main
 to session                        #sends logs to the remote connection.
 no shutdown
 exit
```

### Store logs and debug locally in RAM:
* At Nokia (Alcatel-Lucent) VSR (SR 7750) the logging to the memory (RAM) is activated by default for main issues stream, 
so we don’t have to configure it. Let’s configure the same for the debugging.

```
*A:SR3>config>log# info

log-id 4
 description “DEBUG_TO_MEMORY”
from debug-trace
 to memory                      #saves to memory
 no shutdown
 exit
````

* These logs are stored until the reboot and completely deleted after it. Usually it isn’t problem, because for long-term purposes the logs are stored either at local disc or to syslog server.

### Store logs to local storage:
* Local storage of logs is writing them to the file, which is stored at local HDD/flash. Logs will be saved after reboot.

```
*A:SR3>config>log# info
file-id 1
 location cf3:
 exit
 log-id 10
 from main
 to file 1
 no shutdown
 exit
```



