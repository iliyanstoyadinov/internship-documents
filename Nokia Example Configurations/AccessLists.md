# Access-lists:

## Data plane protection for IPv4:

* Configure separate logging for access-lists - log 101.

```
*A:SR1>config>filter# info
————————–
log 101 create
 destination memory 500
 exit
 ip-filter 1 create
 default-action forward
 description “ACL_SR3_IN”
entry 10 create
 match protocol ospf-igp
 exit
 action forward
 exit
 entry 20 create
 match protocol icmp
 src-ip 10.1.0.3/32
 exit
 log 101
 action drop
 exit
 exit
————————–
*A:SR1>config>router# info
————————–
interface “toSR3”
ingress
 filter ip 1
 exit
 exit
————————–
```

## Data plane protection for IPv6:

* One exception for IPv6 traffic is that we allow NDP packets, which are ARP substitute in IPv6 world. 
Without NDP we are unable to make resolution of MAC addresses to IPv6.

* As IPv4 and IPv6 access-lists live in different name spaces, so it’s OK to use the same name.

```
*A:SR1>config>filter# info
————————–
log 102 create
 destination memory 500
 exit
 ipv6-filter 1 create
 default-action forward
 description “ACL_SR3_IN”
entry 10 create
 match next-header ospf-igp
 exit
 action forward
 exit
 entry 18 create
 match next-header ipv6-icmp
 icmp-type neighbor-solicitation
 exit
 action forward
 exit
 entry 19 create
 match next-header ipv6-icmp
 icmp-type neighbor-advertisement
 exit
 action forward
 exit
 entry 20 create
 match next-header ipv6-icmp
 src-ip fe80::3:1301/128
 exit
 log 102
 action drop
 exit
 exit
————————–
*A:SR1>config>router# info
————————–
interface “toSR3”
ingress
 filter ipv6 1
 exit
 exit
————————–
```

## Access-list based forwarding:

```
*A:SR1>config>filter# info
————————–
ip-filter 1 create
 entry 30 create
 match protocol icmp
 dst-ip 10.1.22.1/32
 src-ip 10.1.33.1/32
 exit
 action forward next-hop 10.2.0.1
 exit
 exit
 ip-filter 10 create
 default-action forward
 entry 10 create
 match
 dst-ip 10.1.22.1/32
 src-ip 10.1.33.1/32
 exit
 action forward
 exit
 exit
 ip-filter 11 create
 default-action forward
 entry 10 create
 match
 dst-ip 10.1.0.1/32
 src-ip 10.1.33.1/32
 exit
 action forward
 exit
 exit
————————–
*A:SR1>config>router# info
————————–
interface “toXR1_p1”
egress
 filter ip 11
 exit
 exit
 interface “toXR1_p2”
egress
 filter ip 10
 exit
 exit
————————–
```