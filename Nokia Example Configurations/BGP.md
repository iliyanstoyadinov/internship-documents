# eBGP and iBGP Configurations:

## iBGP:

```
*A:SR1>config>router>policy-options# info
———————–
prefix-list “PL_IPV4_LO1”
prefix 192.0.2.0/26 exact
 exit
 policy-statement “RP_IPV4_AS65000_LO1_TO_BGP”
entry 10
 from
 protocol direct
 prefix-list “PL_IPV4_LO1”
exit
 action accept
 exit
 exit
 entry 20
 from
 protocol bgp
 exit
 action accept
 exit
 exit
 default-action reject
 exit
———————–
*A:SR1>config>router# info
———————–
autonomous-system 65000
 bgp
 router-id 10.0.255.11
 group “iBGP”
export “RP_IPV4_AS65000_LO1_TO_BGP”
neighbor 10.0.255.33
 family ipv4
 local-address 10.0.255.11
 next-hop-self
 peer-as 65000
 exit
 exit
 no shutdown
 exit
 ```
 
 ## iBGP Route-Reflector:
 
 ```
 *A:SR3>config>router>policy-options# info
———————–
prefix-list “PL_IPV4_LO1”
prefix 192.0.2.128/26 exact
 exit
 policy-statement “RP_IPV4_AS65000_LO1_TO_BGP”
entry 10
 from
 protocol direct
 prefix-list “PL_IPV4_LO1”
exit
 action accept
 exit
 exit
 entry 20
 from
 protocol bgp
 exit
 action accept
 exit
 exit
 default-action reject
 exit
———————–
*A:SR3>config>router# info
———————–
autonomous-system 65000
 bgp
 router-id 10.0.255.33
 group “iBGP”
cluster 10.0.255.33
 export “RP_IPV4_AS65000_LO1_TO_BGP”
neighbor 10.0.255.11
 family ipv4
 local-address 10.0.255.33
 next-hop-self
 peer-as 65000
 exit
 neighbor 10.0.255.22
 family ipv4
 local-address 10.0.255.33
 next-hop-self
 peer-as 65000
 exit
 exit
 no shutdown
 exit
 ```
 * Add cluster X.X.X.X command under group configuration in BGP at route reflector:
 
 ## eBGP:
 
 ```
 *A:SR1>config>router>bgp# info
———————–
group “eBGP”
export “RP_IPV4_AS65000_LO1_TO_BGP”
neighbor 192.0.2.251
 authentication-key Int3r_@$
 local-as 65535 no-prepend-global-as
 peer-as 65100
 exit
 neighbor 192.0.2.253
 authentication-key Int3r_@$
 peer-as 65200
 exit
 exit
———————–
 ```
 
 * Configuration on the route reflector router that is not directly connected with eBGP.
 
 ```
 *A:SR3>config>router>policy-options# info
———————–
prefix-list “PL_IPV4_LO65200”
prefix 198.51.100.0/25 exact
 exit
 policy-statement “RP_IPV4_AS65200_BGP”
entry 10
 from
 protocol direct
 prefix-list “PL_IPV4_LO65200”
exit
 action accept
 exit
 exit
 entry 20
 from
 protocol bgp
 exit
 action accept
 exit
 exit
 default-action reject
 exit
———————–
A:SR3>config>service# info
———————–
vprn 65200 customer 65200 create
 autonomous-system 65200
 bgp
 export “RP_IPV4_AS65200_BGP”
group “eBGP”
neighbor 192.0.2.252
 authentication-key Int3r_@$
 peer-as 65000
 exit
 neighbor 192.0.2.254
 authentication-key Int3r_@$
 peer-as 65000
 exit
 neighbor 198.51.100.255
 authentication-key Int3r_@$
 peer-as 65100
 exit
 exit
 no shutdown
 exit
 exit
———————–
 ```
 
 ## In Nokia (Alcatel-Lucent) we have just two route policies, which control whole operation of BGP. It’s export and import policy:

* Export policy chooses appropriate prefix from routing table (not BGP RIB) based on 
selection criteria (configured under “from” section) and
apply action to the route (configured under “action” section). 
If the action is “reject”, then such route isn’t sent to neighbor. 
If the action is “allow”, the route is sent and you have possibility to modify its path attributes.

* Import policy is used for modification of path attributes for incoming routes and filtering 
their installation in routing table. In any case the routes will be in BGP RIB (Adj-RIB-In, actually).

* For BGP attributes and IPv6 configuration - http://karneliuk.com/2016/09/bgp-between-nokia-alcatel-lucent-sr-and-cisco-ios-xr/



