# Interface Configuration:

* Separation between physical interface - "port" and logical interface - "router interface".

```
A:SR3>config# port 1/1/1
A:SR3>config>port# no shutdown
A:SR3>config>port# ethernet
A:SR3>config>port>ethernet# exit
A:SR3>config>port# exit
A:SR3>config# router
A:SR3>config>router# interface “toXR1”
A:SR3>config>router>if# address 10.0.0.1/24
*A:SR3>config>router>if# port 1/1/1
*A:SR3>config>router>if# no shutdown

```

* LoopBack Interface:

```
*A:SR3# configure
*A:SR3>config# router
*A:SR3>config>router# interface Lo10
*A:SR3>config>router>if$ address 10.0.10.1/32
*A:SR3>config>router>if$ loopback

```