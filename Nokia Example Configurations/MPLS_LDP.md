# MPLS/LDP Configuration:

* In order to turn on any kind of protocol that will form MPLS forwarding plane, we should get routing protocol converged. I use ISIS as our IGP.

```
*A:SR1>config>router>ldp$ info
—————————–
interface-parameters
 interface “toSR3” dual-stack
 ipv4
 no shutdown
 exit
 no shutdown
 exit
 interface “toXR1_p1” dual-stack
 ipv4
 no shutdown
 exit
 no shutdown
 exit
 interface “toXR1_p2” dual-stack
 ipv4
 no shutdown
 exit
 no shutdown
 exit
 exit
 targeted-session
 exit
 no shutdown
—————————–
```

* The router-id from LDP isn’t explicitly configured in Nokia (Alcatel-Lucent) SR OS, because we have configured it initially in global routing configuration.

* To check if LDP session:
`A:SR1# show router ldp session`

* Check the actual labels that are used for MPLS forwarding - LFIB (label forwarding information base):
`A:SR1# show router ldp bindings active`

* Configure the allocation of labels only for host routes:

```
*A:SR1>config>router>policy-options# info
—————————–
prefix-list “PL_LDP_IPV4_PREF”
prefix 0.0.0.0/0 prefix-length-range 32-32
 exit
 policy-statement “RP_HOST_ROUTES”
entry 10
 from
 prefix-list “PL_LDP_IPV4_PREF”
exit
 action accept
 exit
 exit
 default-action reject
 exit
—————————–
*A:SR1>config>router>ldp$ info
—————————–
implicit-null-label
—————————–
```


