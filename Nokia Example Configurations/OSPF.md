# OSPF Configuration:

```
*A:SR3>config>router>ospf# info
router-id 10.0.101.1
area 0.0.0.0
interface “toXR1”
 interface-type point-to-point
 hello-interval 2
 dead-interval 10
 no shutdown
 exit
interface “Lo101”
 no shutdown
 exit
exit
no shutdown
```