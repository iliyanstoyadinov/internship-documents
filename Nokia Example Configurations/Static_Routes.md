# Static route configuration:

```
*A:SR3# configure
*A:SR3>config# router static-route 10.0.11.1/32 next-hop 10.0.0.2
```