# IS-IS

```
*A:SR1>config>router>isis$ info
router-id 10.0.255.1
 area-id 49.0001
 authentication-key !MB@
 authentication-type message-digest
 advertise-passive-only
 ipv6-routing native
 level 1
 wide-metrics-only
 exit
 level 2
 wide-metrics-only
 exit
 interface “system”
passive
 no shutdown
 exit
 interface “toXR1_p1”
level-capability level-2
 hello-authentication-key N0K1@
 hello-authentication-type message-digest
 interface-type point-to-point
 no shutdown
 exit
 interface “toXR1_p2”
level-capability level-2
 hello-authentication-key C1$C0
 hello-authentication-type message-digest
 interface-type broadcast
 no shutdown
 exit
 interface “toSR3”
level-capability level-1
 hello-authentication-key L1_P@$$
 hello-authentication-type message-digest
 interface-type point-to-point
 no shutdown
 exit
 no shutdown
 ```
 
 * Wide-metric is necessary for operation of MPLS traffic-engineering based on RSVP.
Its advantage is overcoming of limitations of low metric, which is only 6 bits,
making possible values for interface cost only in range 1-63.